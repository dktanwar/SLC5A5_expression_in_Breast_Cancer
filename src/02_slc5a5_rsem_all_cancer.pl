#!/usr/bin/perl                                                                                                                                                                                                                             
use strict;
use warnings;

print join("\t", "#Cancer", "TCGA_id", "Gene", "Expression"), "\n";

my $file = shift;
open(my $cancer, "bzcat -c $file |") || die "Could not open the file $file\n";

while(my $line = <$cancer>){
    chomp $line;
    next if $line =~ /^\#/;
    my @split = split(/\t/, $line);
    next unless $split[2] eq "SLC5A5|6528";
    print join("\t", $split[0], $split[1], $split[2], $split[3]), "\n";
}
close ($cancer);

