#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;

my $file = shift;
open(my $brca, "bzcat -c $file |") || die "Could not open the file $file\n";

my $aa_count;
my %aa;

while(my $line = <$brca>){
    chomp $line;
    next if $line =~ /^\#/;
    my @split = split(/\t/, $line);
    next unless $split[0] eq "BRCA";
    $aa_count->{$split[2]}->{$split[1]} = $split[3];
    $aa{$split[1]} = 1;
}
close ($brca);

my @aa_list = sort keys %aa;
print join("\t", "#Genes", @aa_list), "\n";
foreach my $gene (sort keys %{$aa_count}){
    my @line;
    push @line, $gene;
    foreach my $sample (@aa_list) {
	if (exists $aa_count->{$gene}->{$sample}) {
	    push @line, $aa_count->{$gene}->{$sample};
	}
    }
    print join("\t", @line), "\n";
}
